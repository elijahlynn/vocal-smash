# Vocal Smash

Get quick, visual, useful feedback on hitting your notes with Vocal Smash! 

Vocal Smash is a nifty singing app for serious singers and vocalists!

Vocal Smash is based on scientific research in the book Peak. One of the premises in the book is that the closer you get to instant feedback, then the more feedback cycles you can do in a given time, and this causes all types and ages of people self-learn at a faster pace, just by observing if they did it right or not. People have the ability to self-learn on if they get fast enough feedback. 

## Features:

**MVP:**

* Pinch to zoom in on your range. 

* Hold to freeze, tap to unfreeze: Sometimes you just want to sing a short riff then see how you did. 


**Post-MVP:**

* Game: Compete with others and yourself with your "Singing Score".  others can upload their accapella recordings and you can compete against others for the best singing score which is based on how well you match the singer. 
* Gestures: Swipe away or towards the animation flow direction to slow down or speed up the visualization flow.


* Grid: A light grid to give feedback on what note is being targeted when it is farther away from the source number row. 

* TBD: Possibly add a number row at the end for a sandwhich effect too

* Visual: Put real sharps or flats in the gap between the whole notes.

* Resolution: Don't really want to expose this feature. If requested a lot, then I will, but I think the elegance of this app is also going to be in the settings and the interaction too. 

* Color scheme: Adjust color schemes. This is way in the future, if I do it. 




I built the ultimate singing feedback tool for myself and think others will love it too. Check the video out!

My name is Elijah Lynn and I am an open source software engineer. I've been teaching myself how to sing and have been looking for a tool to give me pitch feedback quickly. I haven't found what I have been looking for and I came across the open source project [YouTube Musical Spectrum](https://github.com/mfcc64/youtube-musical-spectrum) by Muhammad Faiz based on the showcqt.js library, also created by Muhammad. 
